#!/bin/bash
#: Title       : miniFantasyTheater renderfarm
#: Author      : David REVOY < info@davidrevoy.com >
#: License     : GPLv3 or higher

clear

# Title
printf "\033]0;%s\007\n" "miniFantasyTheater renderfarm"

# User configuration
export defaultLang="en"
export creditSuffix="_miniFantasyTheater"
# An absolute path to a directory (ending by /) to save snapshots at each rendering.
export backupPath="/media/deevad/virgo/roaming/"

# Directories name
export langDir="lang"
export lowResDir="low-res"
export hiResDir="hi-res"
export gfxOnlyDir="gfx-only"
export txtOnlyDir="txt-only"
export thumbDir="thumbnails"
export databaseDir="database"
export wipDir="wip"
export cacheDir="cache"

# Export standards
export lowResSize="1080x" # width in px (height will be proportional)
export lowResPpi="96" # in ppi. Classic values: 72, 96(default)
export hiResPpi="650" # in ppi. Classic values: 72, 96(default)
export printWidth="150" # PDF export require a physical size, in mm
export printHeight="150" # PDF export require a physical size, in mm

# Utilities
export dirPath="${PWD}"
version=$(date +%Y-%m-%d_%Hh%M%S)
export version
export Blue=$'\e[1;34m'
export Yellow=$'\e[1;33m'
export Green=$'\e[1;32m'
export Red=$'\e[1;31m'
export Purple=$'\e[1;35m'
export Off=$'\e[0m'

# Directory structure
_makeDir (){
  if [ ! -d "$1" ]; then
    echo "* create new directory: $1"
    mkdir -p "$1"
  fi
}

# Library to delete a file with better security than rm
_deleteSecure () {
  pathToDelete=$1
  # exit if empty
  if [ -z "${pathToDelete}" ]; then
    echo "${Red} security exit: a variable for deleting is undefined or dangerous.${Off}"
    exit 1
  fi

  if [ -f "${pathToDelete}" ]; then
    rm "${pathToDelete}"
  fi

  if [ -d "${pathToDelete}" ]; then
    rm -r "${pathToDelete}"
  fi
}

# Check if dependencies $1, $2 ,$3 ... exist.
_dependency_check () {
    for arg; do
      if command -v "$arg" >/dev/null 2>&1 ; then
        true
      else
        echo "${Red}* Error:${Off} missing dependency: ${Red}$arg${Off} not found."
        echo "Fatal: missing dependency: $arg."
        exit
      fi
    done
}

# Library to check on Inkscape rendering (because it is not reliable at all)
_inkscapeRender () {
  input=$1
  ouput=$2

  # workaround one: a bug: https://gitlab.com/inkscape/inkscape/-/issues/4716#note_1898150983
  microtime=$(date +%s)
  export SELF_CALL=$microtime

  # workaround two:
  inkscape "${input}" -o "${ouput}" > /dev/null
  if [ ! -f "${ouput}" ]; then
    echo "Rendering failed, retrying."
    inkscape "${input}" -o "${ouput}" > /dev/null
    if [ ! -f "${ouput}" ]; then
      echo "Rendering failed a second time, retrying."
      inkscape "${input}" -o "${ouput}" > /dev/null
      if [ ! -f "${ouput}" ]; then
        echo "Rendering failed a third time, retrying a last one, after 3 seconds."
        sleep 3
        inkscape "${input}" -o "${ouput}" > /dev/null
        if [ ! -f "${ouput}" ]; then
          echo "${Red}Error: Inkscape couldn't render something even after three time.${Off}"
        fi
      else
        echo "Success: Inkscape render (3rd time)."
      fi
    else
      echo "Success: Inkscape render (2nd time)."
    fi
  fi
}

_renderKraComicPages(){
  inFile="$1" # file gets in with an absolute path

  inFilename=${inFile##*/}
  kraFile=${inFilename}
  kraFileNoExt="${kraFile%.*}"

  txtFile="${kraFileNoExt}.txt"
  pngFile="${kraFileNoExt}.png"
  svgFile="${kraFileNoExt}.svg"
  jsonFile="${kraFileNoExt}.json"
  jpgFile="${kraFileNoExt}.jpg"
  kraTmpDir="${kraFileNoExt}-${version}"
  jpgFileWip="${kraFileNoExt}_${version}.jpg"
  renderMeFile="${kraFileNoExt}-renderme.txt"
  jpgFileCredit="${kraFileNoExt}${creditSuffix}.jpg"
  jpgFileThumbnail="${kraFileNoExt}-thumb${creditSuffix}.jpg"

  # Checksum of *.kra file.
  kraMd5sum=$(md5sum "${dirPath}/${kraFile}")

  # Create a tokken if none is available, so later grep can compare with empty file.
  if [ ! -f "${dirPath}/${cacheDir}/${txtFile}" ]; then
    touch "${dirPath}/${cacheDir}/${txtFile}"
  fi

  # Compare if actual *.kra checksum is similar to the previous one recorded on txtFile.
  if grep -q "${kraMd5sum}" "${dirPath}/${cacheDir}/${txtFile}"; then
    # Nothing to do, kraFile is up-to-date.
    true
  else
    echo "${Yellow}[${kraFile}] -> Rendering. ${Off}"

    # Save a token to inform the SVG renderer later to re-render the txt for this page.
    touch "${dirPath}/${cacheDir}/${renderMeFile}"

    # Update the cache.
    md5sum "${dirPath}/${kraFile}" > "${dirPath}/${cacheDir}/${txtFile}"

    # Extract the hi-resolution PNG directly from *.kra file.
    mkdir -p "/tmp/${kraTmpDir}"
    unzip -j "${dirPath}/${kraFile}" "mergedimage.png" -d "/tmp/${kraTmpDir}" > /dev/null
    # Sanify the PNG (no Alpha, compression, sRGB colorspace)
    convert "/tmp/${kraTmpDir}/mergedimage.png" -colorspace sRGB -background white -alpha remove -define png:compression-strategy=3 -define png:compression-level=9 "${dirPath}/${cacheDir}/${pngFile}"

    # Update the preview in the langDir, for translation
    convert "${dirPath}/${cacheDir}/${pngFile}" -resize "${lowResSize}" -density "${lowResPpi}" -units pixelsperinch -unsharp 0x0.50+0.50+0 -colorspace sRGB -quality 85% "${dirPath}/${langDir}/${jpgFile}"

    # Update the work in progress (wip) snapshot.
    convert "${dirPath}/${cacheDir}/${pngFile}" -colorspace sRGB -density "${hiResPpi}" -quality 95% "${dirPath}/${wipDir}/${jpgFileWip}"

    # Copy a version to the graphic only directory
    cp -rf "${dirPath}/${wipDir}/${jpgFileWip}" "${dirPath}/${hiResDir}/${gfxOnlyDir}/${jpgFileCredit}"

    # Create a thumbnail, crop a panel
    convert "${dirPath}/${cacheDir}/${pngFile}" -crop 1712x1712+171+162 -resize "${lowResSize}" -density "${lowResPpi}" -units pixelsperinch -unsharp 0x0.50+0.50+0 -colorspace sRGB -quality 89% "${dirPath}/${lowResDir}/${thumbDir}/${jpgFileThumbnail}"

    # Copy the file to create a backup of the current version.
    cp "${dirPath}/${kraFile}" "${backupPath}/${version}_${kraFile}"

    # Get size info of the original
    imgWidth=$(identify -format "%w" "${dirPath}/${cacheDir}/${pngFile}")> /dev/null
    imgHeight=$(identify -format "%h" "${dirPath}/${cacheDir}/${pngFile}")> /dev/null

    # Remove the tmp folder.
    _deleteSecure "/tmp/${kraTmpDir}"

    # SVG template for translation
    if [ ! -f "${dirPath}/${langDir}/${defaultLang}/${svgFile}" ]; then
      cat > "${dirPath}/${langDir}/${defaultLang}/${svgFile}" << EOL
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!-- Created with Inkscape (http://www.inkscape.org/) -->

<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:xlink="http://www.w3.org/1999/xlink"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   id="svg2"
   version="1.1"
   inkscape:version="0.1.3 (bash-generated)"
   width="${imgWidth}"
   height="${imgHeight}"
   sodipodi:docname="page.svg">
  <metadata
     id="metadata8">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title></dc:title>
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <defs
     id="defs6" />
  <sodipodi:namedview
     pagecolor="#444444"
     bordercolor="#000000"
     borderopacity="1"
     objecttolerance="10"
     gridtolerance="10"
     guidetolerance="10"
     inkscape:pageopacity="0"
     inkscape:pageshadow="2"
     inkscape:window-width="1920"
     inkscape:window-height="1050"
     id="namedview4"
     showgrid="false"
     inkscape:zoom="0.15037482"
     inkscape:cx="-900.65922"
     inkscape:cy="1491.8983"
     inkscape:window-x="1920"
     inkscape:window-y="0"
     inkscape:window-maximized="1"
     inkscape:current-layer="layer1" />
  <g
     inkscape:groupmode="layer"
     id="layer2"
     inkscape:label="artwork"
     sodipodi:insensitive="true">
    <image
       xlink:href="../${jpgFile}"
       y="0"
       x="0"
       id="${kraFileNoExt}"
       height="${imgHeight}"
       width="${imgWidth}" />
  </g>
<g
     inkscape:groupmode="layer"
     id="layer3"
     inkscape:label="speechbubbles" />
  <g
     inkscape:groupmode="layer"
     id="layer1"
     inkscape:label="txt">
    <text
       id="shape013"
       text-rendering="auto"
       fill="#000000"
       stroke-opacity="0"
       stroke="#000000"
       stroke-width="0"
       stroke-linecap="square"
       stroke-linejoin="bevel"
       letter-spacing="0"
       text-anchor="middle"
       word-spacing="0"
       style="font-size:123.063px;font-family:'Grand Hotel';text-align:start;display:inline"
       x="175.27139"
       y="3776.3242"
       inkscape:label="Credit-Title"><tspan
         x="588.22894"
         id="tspan1"
         style="stroke-width:0">Mini Fantasy Theater</tspan></text>
    <text
       id="shape2"
       text-rendering="auto"
       fill="#000000"
       stroke-opacity="0"
       stroke="#000000"
       stroke-width="0"
       stroke-linecap="square"
       stroke-linejoin="bevel"
       letter-spacing="0"
       text-anchor="end"
       word-spacing="0"
       style="font-size:73.8377px;font-family:Pangolin;text-align:start;display:inline"
       x="3040.1685"
       y="3736.7832"
       inkscape:label="Credit-Name"><tspan
         x="3680.696"
         id="tspan2"
         style="stroke-width:0">David Revoy</tspan><tspan
         x="3680.696"
         dy="73.837677"
         id="tspan3"
         style="stroke-width:0">www.davidrevoy.com</tspan></text>
    <text
       id="shape21"
       text-rendering="auto"
       fill="#8e8b88"
       stroke-opacity="0"
       stroke="#000000"
       stroke-width="0"
       stroke-linecap="square"
       stroke-linejoin="bevel"
       letter-spacing="0"
       text-anchor="end"
       word-spacing="0"
       style="font-size:45px;line-height:100%;font-family:Pangolin;text-align:start;display:inline"
       x="1167.5646"
       y="3778.4343"
       inkscape:label="Credit-License"><tspan
         sodipodi:role="line"
         id="tspan10"
         x="1922.5271"
         y="3778.4343"
         style="font-size:45px;line-height:100%;text-align:center;text-anchor:middle"><tspan
           x="1922.5271"
           id="tspan4"
           style="font-size:45px;line-height:100%;text-align:center;text-anchor:middle;stroke-width:0">license: Creative Commons Attribution-ShareAlike 4.0 International</tspan></tspan></text>
    <text
       id="shape22"
       text-rendering="auto"
       fill="#828181"
       stroke-opacity="0"
       stroke="#000000"
       stroke-width="0"
       stroke-linecap="square"
       stroke-linejoin="bevel"
       kerning="none"
       letter-spacing="0"
       text-anchor="middle"
       word-spacing="0"
       style="font-size:73.8377px;font-family:'Patrick Hand SC';text-align:start;display:inline"
       x="1701.0232"
       y="3732.3135"
       inkscape:label="Credit-ID"><tspan
         x="1922.2478"
         id="tspan5"
         style="stroke-width:0">Episode #%</tspan></text>
  </g>
</svg>
EOL
    fi

    # Database file template
    if [ ! -f "${dirPath}/${databaseDir}/${jsonFile}" ]; then
      cat > "${dirPath}/${databaseDir}/${jsonFile}" << EOL
{
  "published": "2024-00-00",
  "pluxmlID": 0,
  "mastodonID": 0,
  "credits": {
    "art":[
      "David Revoy <https://framagit.org/Deevad>"
    ],
    "scenario":[
      "David Revoy <https://framagit.org/Deevad>"
    ]
  }
}
EOL
    fi

  fi
}

_parralalComputeKra()
{
  export -f _renderKraComicPages
  export -f _deleteSecure
  find "${dirPath}"/*.kra -maxdepth 1 | parsort | parallel --keep-order _renderKraComicPages "{}"
}

_renderSvgComicPages(){
  inFile="$1" # file gets in with an absolute path

  inPath=${inFile%/*}
  svgFile=${inFile##*/}
  svgFileNoExt="${svgFile%.*}"

  lang=${inPath//"${dirPath}/${langDir}/"/}
  microtime=$(date +%s)
  svgTmpDir="${svgFileNoExt}-${microtime}"
  pngFile="${svgFileNoExt}.png"
  pngFileCredit="${svgFileNoExt}${creditSuffix}.png"
  pdfFileCredit="${svgFileNoExt}${creditSuffix}.pdf"
  pngFileSanified="${svgFileNoExt}_sanified.png"
  txtFile="${svgFileNoExt}_${lang}.txt"
  renderMeFile="${svgFileNoExt}-renderme.txt"
  jpgFile="${svgFileNoExt}.jpg"
  jpgFileCredit="${svgFileNoExt}${creditSuffix}.jpg"
  rMarkdownFile="${svgFileNoExt}data.md"

  # Checksum of *.svg file.
  svgMd5sum=$(md5sum "${inFile}")

  # Create a tokken if none is available, so later grep can compare with empty file.
  if [ ! -f "${dirPath}/${cacheDir}/${txtFile}" ]; then
    touch "${dirPath}/${cacheDir}/${txtFile}"
  fi

  # Compare if actual *.svg checksum is similar to the previous one recorded on txtFile.
  if grep -q "${svgMd5sum}" "${dirPath}/${cacheDir}/${txtFile}"; then
    # Nothing to do, svgFile is up-to-date.
    true
  else
    touch "${dirPath}/${cacheDir}/${renderMeFile}"
  fi

  # Check if there is not a ready made renderme token ready
  if [ ! -f "${dirPath}/${cacheDir}/${renderMeFile}" ]; then
    true
  else
    echo "${Yellow}[${svgFile}] -> Rendering. ${Off}"

    # Update the cache.
    md5sum "${inFile}" > "${dirPath}/${cacheDir}/${txtFile}"

    # Render the sRGB composited (text and artworks) full resolution PNG
    mkdir -p "/tmp/${svgTmpDir}/${lang}"
    cp -rf "${inFile}" "/tmp/${svgTmpDir}/${lang}/${svgFile}"
    sed -i 's/jpg"/png"/g' "/tmp/${svgTmpDir}/${lang}/${svgFile}"
    cp -rf "${dirPath}/${cacheDir}/${pngFile}" "/tmp/${svgTmpDir}/${pngFile}"
    
    _inkscapeRender "/tmp/${svgTmpDir}/${lang}/${svgFile}" "/tmp/${svgTmpDir}/${lang}/${pngFile}"
    convert "/tmp/${svgTmpDir}/${lang}/${pngFile}" -colorspace sRGB -background white -alpha remove -define png:compression-strategy=3 -define png:compression-level=9 "/tmp/${svgTmpDir}/${lang}_${pngFileSanified}"

    # Render an high resolution jpg
    convert "/tmp/${svgTmpDir}/${lang}_${pngFileSanified}" -colorspace sRGB -density "${hiResPpi}" -units pixelsperinch "${dirPath}/${hiResDir}/${lang}_${jpgFileCredit}"

    # Render a low-res version to share on the web
    convert "/tmp/${svgTmpDir}/${lang}_${pngFileSanified}" -resize "${lowResSize}" -density "${lowResPpi}" -units pixelsperinch -unsharp 0x0.50+0.50+0 -colorspace sRGB -quality 89% "${dirPath}/${lowResDir}/${lang}_${jpgFileCredit}"

    # Render a transparent raster PNG version of the text
    cp -rf "/tmp/${svgTmpDir}/${lang}/${svgFile}" "/tmp/${svgTmpDir}/${lang}/${svgFileNoExt}_backgroundless.svg"
    sed -i 's/<image[^<]*//' "/tmp/${svgTmpDir}/${lang}/${svgFileNoExt}_backgroundless.svg"

    # Render a transparent vector PDF version of the text
    _inkscapeRender "/tmp/${svgTmpDir}/${lang}/${svgFileNoExt}_backgroundless.svg" "${dirPath}/${hiResDir}/${txtOnlyDir}/${lang}_${pdfFileCredit}"
    pdfjam --outfile "${dirPath}/${hiResDir}/${txtOnlyDir}/${lang}_${pdfFileCredit}" --papersize "{${printWidth}mm,${printHeight}mm}" "${dirPath}/${hiResDir}/${txtOnlyDir}/${lang}_${pdfFileCredit}" > /dev/null 2>&1
    # For debugging size:
    #pdfinfo "${dirPath}/${exportDir}/${svgFileNoExt}_${langDir}.pdf" | grep 'Page size' | awk '{printf "Page size: %.2f x %.2f mm\n", $3*25.4/72, $5*25.4/72}'

    # Copy the file to create a backup of the current version.
    cp "${dirPath}/${langDir}/${lang}/${svgFile}" "${backupPath}/${version}_${svgFile}"

    _deleteSecure "${dirPath}/${cacheDir}/${renderMeFile}"
    _deleteSecure "/tmp/${svgTmpDir}"

    # R Markdown template
    if [ ! -f "${dirPath}/${langDir}/${lang}/${rMarkdownFile}" ]; then
      cat > "${dirPath}/${langDir}/${lang}/${rMarkdownFile}" << EOL
---
translation:
  - original version
proofreader:
  - name
---

## Title here

A webcomic in four panels:

Panel 1:
> character: Hello World!

Panel 2:

Panel 3:

Panel 4:
EOL
    fi

  fi
}

_parralalComputeSvg()
{
  export -f _renderSvgComicPages
  export -f _deleteSecure
  export -f _inkscapeRender
  find "${dirPath}/${langDir}"/*/*.svg | parsort | parallel --keep-order --delay 0.1 _renderSvgComicPages "{}"
}


echo "${Blue}--------------------------------${Off}"
echo "${Blue}| miniFantasyTheatre renderfarm |${Off}"
echo "${Blue}--------------------------------${Off}"

echo ""
_dependency_check pdfjam convert inkscape md5sum

echo "* target path: ${dirPath}"
_makeDir "${dirPath}/${langDir}"
_makeDir "${dirPath}/${langDir}/${defaultLang}"
_makeDir "${dirPath}/${lowResDir}"
_makeDir "${dirPath}/${lowResDir}/${thumbDir}"
_makeDir "${dirPath}/${hiResDir}"
_makeDir "${dirPath}/${hiResDir}/${gfxOnlyDir}"
_makeDir "${dirPath}/${hiResDir}/${txtOnlyDir}"
_makeDir "${dirPath}/${databaseDir}"

_makeDir "${dirPath}/${wipDir}"
_makeDir "${dirPath}/${cacheDir}"

echo ""
echo "${Blue}Krita Files:${Off}"
_parralalComputeKra

echo ""
echo "${Blue}Svg Files:${Off}"
_parralalComputeSvg

# [Regenerate Timestamp]
date +'%s' > "${dirPath}/${databaseDir}/last_updated.txt"
date +'%d/%m/%Y%t%H:%M:%S' >> "${dirPath}/${databaseDir}/last_updated.txt"

echo "${Blue}Done.${Off}"

# Debug: running cache-less (render all each time, for testing purpose).
#_deleteSecure "${dirPath}"/"${cacheDir}"

# Invite to reload the script. Bypass if called with --noprompt flag.
if [ "$1" = "--noprompt" ]; then
  echo ""
else
  echo ""
  echo "${Purple}  Press [Enter] to launch the script again or:"
  echo "[q]${Off} => to Quit.${Off}"
  read -r -p "?" ANSWER
  if [ "$ANSWER" = "q" ]; then
    exit
  else
    # Launch again
    bash "${dirPath}/mft_renderfarm.sh"
  fi
fi
