# Webcomics: Mini Fantasy Theater (archived)

---

### ⚠️ This repository has moved: it has been merged into [https://framagit.org/peppercarrot/webcomics](https://framagit.org/peppercarrot/webcomics). It is now archived (and will be deleted in 2025).

---


This repository contains all the files needed to translate the miniFantasyTheater webcomics.

For translating you'll mainly need
- The installed [fonts](https://framagit.org/peppercarrot/fonts), mainly `Deevadhand` in the Latin subdirectory and in normal, bold and italic variants.
- [Inkscape](https://inkscape.org/) to open and edit the SVGs in the `lang` directory.
- A text editor (not rich) to edit the markdown files ending with _transcript.md.

## Getting started

- Download or clone the repository.
- Duplicate the `en` directory and rename it with [two letters](https://framagit.org/peppercarrot/webcomics/-/blob/master/langs.json?ref_type=heads) for your target language (e.g. fr, nl, vi...).
- Edit and translate the files inside.
  * The svg files with Inkscape.
  * The [R markdown](https://bookdown.org/yihui/rmarkdown-cookbook/rmarkdown-anatomy.html) files (YAML for the credits on the top, and markdown for the Alt/transcript under) with a text-editor. It will follow the same convention as the [Pepper&Carrot JSON](https://www.peppercarrot.com/en/documentation/081_info.json.html) for the available keys and data. This documentation is still on my TODO list.
- Send it back here as a merge request (you need a Framagit account), or by email*.

<small>
\* In case of email: send it to info(at)davidrevoy(dot)com, please include the keyword `MFT-git` in the object, and just attach your directory as a zip. Also inform me in your email of a `username` and `email` that can be made public.
</small>

## License

**By submitting content to this repository, you agree to submit your work under the same license.**  

Content in this repository is licensed under the [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en).

## Code of Conduct

You agree to follow the rules of [our Code of Conduct](https://www.peppercarrot.com/en/documentation/409_Code_of_Conduct.html) if you wish to participate in the Mini Fantasy Theater web comic, Pepper and Carrot, and related projects.

## Credits

Each translation directory contains a file named [info.json](https://www.peppercarrot.com/en/static14/documentation&page=081_info.json).

## Artwork sources

This repository does not contain any `\*.kra` artwork source files. They are simply too heavy to be part of the versioning system. You'll only find low quality JPGs here to preview while editing translations.
Do not edit or open merge requests for the JPG here.

The Krita source files will be available soon.

## Episode metadata

Each episode contains a JSON metadata file in the database directory.
This doesn't need to be edited by translators.

## Need help?

You can ask questions in our [chat room](https://matrix.to/#/#peppercarrot:matrix.org).
