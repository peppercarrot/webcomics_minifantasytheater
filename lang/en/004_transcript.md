## Accessories

A webcomic in four panels:

Panel 1. A young dark queen ask to a magic mirror on the wall
> Dark queen: Mirror mirror on the wall, who's the prettiest of them all?

Panel 2. The mirror shows a picture of a young lady.
> Mirror: Her.

Panel 3. The queen, a little frustrated, turns her back to the mirror, ready to leave the room.
> Dark Queen: Maybe with some accessories I can convince you it's me!
> Mirror: Well, I doubt it....

Panel 4. The Queen is back with a big hammer, she smiles. The mirror sweats with discomfort and shake while displaying the portrait of the Dark Queen.
> Mirror: Oh! **That** kind of accessory! **Very** convincing! 
