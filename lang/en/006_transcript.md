## Help desk

A webcomic in four panels:

Panel 1. A young warrior, with the back of his battle dress partially burned, walks angrily toward the sword shop, holding the broken sword in his hand.

Panel 2. Inside, the warrior is manifesting anger, the blade and guard, which have been placed on the counter to show how it is obviously broken. The blacksmith carefully examines the broken sword. 

Panel 3. The blacksmith carefully examines even more the broken sword, with expertize, handling it. The warrior is curious.

Panel 4. The blacksmith did put back the blade and guard on the counter. The warrior is exasperated.
> Blacksmith: Have you tried turning it off and on again? 
