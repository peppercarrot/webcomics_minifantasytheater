## Dunning–Kruger effect

A webcomic in four panels:

Panel 1: A very young knight, poorly equipped (oversized pants, topless, wooden sword and shield, rusty helmet), screams with overconfidence in front of a cave on a foggy night.
> Knight: Master of Dragons! I challenge you to a duel, against ME, the greatest knight the kingdom has ever known!!! My knowledge of fencing is unrivaled!!!

Panel 2: The dragon walks calmly out of the cave, only the large nose is visible while his head is still in shadow. Not a fierce look, but a massive size. He is just tired of this kind of interruption. The knight continues:
> Knight: You're terrified, aren't you?! well, you should be! I'm terrifying! Show Yourself!!! Coward!!!

Panel 3: The camera shows an oversized dragon, the knight is the size of a mosquito, attacking his toenails. The dragon just look at the situation with thinking. And an epic full moon illuminates the scene.
> Knight: Hyah! Hyah!
> Partner of the dragon (off panel, from the cave): Honey, who is that?

Panel 4: A close-up of the dragon, turning his head towards the cave to answer his partner:
> Master of Dragons: It's no one. As usual: an overconfident newbie, victim of overestimating his own abilities. A classic bias in human nature.
> Knight (off panel): Hyah! Hyah!
