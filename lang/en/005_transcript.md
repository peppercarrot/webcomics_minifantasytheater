## Too often

A webcomic in four panels:

Panel 1. A battle is raging between a ferocious dragon and a warrior with a magic sword in flames. The scene takes place in a dense pine forest, in the golden light of a late afternoon, before dusk.

Panel 2. Close-up of the sword in battle, blocking an attack from the dragon's claws. But the warrior's face is suddenly distracted: his sword is speaking a message!
> Sword: We've got an update for you. **Installing!**
> Warrior: Eeeek!

Panel 3. The warrior steps away from the battle, looking to his sword in panic. It is surrounded by rays of magic coming from a distance. They illuminate the scene in blue. The dragon doesn't immediately understand the situation, he is fascinated by this event.
> Sword: 80% ... 90%...
> Warrior: No no no no no!

Panel 4. The warrior looks at the result with tears in his eyes: his sword is broken. The blade now lies on the ground, unusable. His fate is sealed. The dragon understands that he has won, he gets a fierce red look in his eyes and prepares a fireball in his smiling mouth. The sword continues to speak, the text is distorted.
> Sword: Update Completed. Enjoy! 
