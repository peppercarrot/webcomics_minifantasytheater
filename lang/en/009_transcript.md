## Art Block

A wordless webcomic in four panels:

Panel 1: A small fairy with brown hair and ordinary transparent wings (a bit like a dragonfly, but less beautiful) sees a proud fairy, beautiful, with colorful butterfly wings. She's ecstatic at the sight.

Panel 2: Later at home, in front of her mirror, as the sun sets, our fairy looks at herself and her wings, depressed.

Panel 3: At night, she lies down on her bed in a phoetal position, crying.

Panel 4: The next day, on the bed, same shot, but instead of the fairy a huge silk cocoon. It's morning light, and the sun rays are shining on it. This panel is full of hope and announces a future metamorphosis.
