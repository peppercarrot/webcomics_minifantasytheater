## Morning Routine

A webcomic in four panels:

Panel 1: Pepper sleeps in a cozy bed, her haircut is clumsy, she is slightly irritated. It's early morning. She is surrounded with spell books, an alarm clock, carrot is also on the bed.
> SoundFx (alarm): Ring! Ring! Ring!

Panel 2: Pepper walks in the stairs like a zombie in her nightdress. A gentle cloud of flavor guide her.

Panel 3. Still foggy, she is ready jumps on a DIY wooden hook of a swimming pool, many books are arranged under to make it like a stair. The smell comes clearly underneath her.

Panel 4. She barely floats face downward, on a huge cup of coffee. Carrot watches surprised on the top of the plank.
> Pepper: BLOOP BLOOP BLOOP BLOOP.
