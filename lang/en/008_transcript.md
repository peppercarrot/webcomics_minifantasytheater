## Carnivorous Plants

A wordless webcomic in four panels:

Panel 1 : A warrior descends the steps of a Colimasson staircase. She passes a strange room made of brick and stone, typical of dungeons. We see her in the frame of an arched doorway, surprised by what she sees: in the foreground, exotic green leaves and vines indicate that the room is infested with a carnivorous plant. Also in the foreground is the silhouette of a victim.

Panel 2 : Contre champs, she arrives, alarmed, with the help of a young adventurer whose head is completely trapped by a large bulbous flower.

Panel 3 : As she prepares to cut the vines with her sword, she is surprised: the captive's hands beckon her to stop!

Panel 4 : Wide shot: The flower releases the head of her victim: she's a young elf with a beautiful haircut and a perfect blow-dry. She looks at herself in a small portable mirror. The warrior is stunned.
