## Mon trésor

Un webcomic en 4 cases :

Case 1 : Dans une grotte contenant un trésor avec beaucoup d'or, une jeune aventurière est chassée par un monstre : un coffre à trésors avec de grandes dents. Elle jette tous les objets en or collectés qu'elle tenait dans sa fuite.

Case 2 : Pendant qu'elle grimpe une montagne d'os et de crânes pour atteindre la sortie de la grotte, l'aventurière jette un os sur le monstre. Il est surpris. 

Case 3 : Comme un brave chien, le monstre rend l'os à l'aventurière, content et impatient de recommencer. L'aventurière se met à réfléchir.

Case 4 : Plus tard, dans la ville, l'aventurière marche fièrement et joyeusement avec son nouveau "chien" (le coffre à trésors) tenu en laisse. Il a l'air très heureux. Les personnes autour semblent surpris et apeurés. 
