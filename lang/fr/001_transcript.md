## Routine Matinale

Un webcomic en quatre cases :

Case 1 : Pepper se réveille dans un lit confortable, ses cheveux sont désordonnés, elle est un peu agacée. Elle est entourée de livres de sorts, d'un réveil, et Carrot est aussi sur le lit.
> Effet sonore (alarme) : Ring ! Ring ! Ring !

Case 2 : Pepper descend les escaliers an bâillant dans sa chemise de nuit. Une petite odeur la guide.

Case 3 : Toujours à moitié endormie, elle s'apprête à sauter depuis un plongeoir en bois fait-maison dans une piscine. Beaucoup de livres sont disposés en-dessous pour former un petit escalier. L'odeur vient clairement de dessous elle. 

Case 4 : Elle flotte à peine sur le ventre, dans une tasse de café géante. Carrot la regarde, surpris, depuis le plongeoir.
> Pepper : BLOUP BLOUP BLOUP BLOUP.
