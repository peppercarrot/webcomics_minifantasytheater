## Plantes carnivores

Un webcomic sans dialogue en quatre cases :

Case 1 : Une guerrière descend les marches d'un escalier en colimaçon. Elle arrive dans une étrange pièce faite de briques et de roches, typique d'un donjon. Nous la voyons à l'entrée d'un passage en forme d'arc cintré, surprise par ce qu'elle voit : au premier plan, des feuilles vertes exotiques et des vignes indiquent que la pièce est infestée de plantes carnivores. Il y a également la silhouette d'une victime.

Case 2 : Contrechamp, la guerrière s'approche, en alerte, pour aider la jeune aventurière dont la tête est totalement piégée dans une large fleur rouge en forme de bulbe. 

Case 3 : Alors qu'elle s'apprête à couper les vignes avec son épée, elle est surprise : les mains de la victime lui font signe de s'arrêter ! 

Case 4 : Plan large : la fleur relâche la tête de la victime : c'est une jeune elfe avec une jolie coupe de cheveux et un brushing parfait. Elle se regarde dans un petit miroir portatif. La guerrière est étonnée.
