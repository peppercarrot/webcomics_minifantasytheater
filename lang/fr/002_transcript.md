## Monstre

Un webcomic en 4 cases :

Case 1 : Un adorable mage noir est agenouillé devant un cercle de bougies et une lampe magique. La lampe commence à émettre un nuage de fumée. 
> Adorable mage noir : Ô, Génie des Ténèbres, Seigneur des Vœux, je te somme d'apparaître !

Case 2 : Le nuage se transforme en une créature élémentaire des ténèbres avec des cornes et des yeux rouges, le Génie des Ténèbres.
> Génie des Ténèbres : Quel est ton vœu, Ô humain ?
> Adorable mage noir : Je souhaite un monstre dépourvu de moralité, avec une précision létale, une vision nocturne, et des griffes acérées. 

Case 3 : Gros plan sur le visage du Génie. Il lance un sort lumineux. 
> Génie des Ténèbres : Tes désirs sont des ordres.

Case 4 : Le jeune mage noir est étonné du résultat : il a reçu un petit chat. Le Génie disparaît. 
