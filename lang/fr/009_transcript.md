## Panne d'inspiration

Un webcomic sans dialogue en quatre cases :

Case 1 : Une petite fée avec des cheveux bruns et des ailes transparentes ordinaires (comme celles des libellules, mais moins belles) voit une autre fée, jolie et fière, avec des ailes de papillons colorées. La petite fée est extatique en la voyant.

Case 2 : Plus tard, chez elle, devant son miroir, notre petite fée regarde tristement son reflet et ses ailes, alors que le soleil se couche.

Case 3 : La nuit venue, elle est allongée sur son lit en position fœtale, en pleurs.

Case 4 : Le jour suivant, dans le lit, même plan de caméra, mais à la place de la petite fée se trouve un grand cocon de soie. C'est le matin, et les rayons du soleil éclairent le cocon. Cette case est porteuse d'espoir et annonce une future métamorphose.
